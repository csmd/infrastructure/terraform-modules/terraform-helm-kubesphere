variable "kubesphere_helm_repository" {
  type = string
  default = "https://charts.kubesphere.io/main"
}

variable "kubesphere_namespace" {
  description = "Kubesphere Namespace"
  type = string
  default = "kubesphere-system"
}

variable "cluster_endpoint" {
  description = "Kubernetes Cluster Endpoint"
  type = string
}

variable "cluster_token" {
  description = "Kubernetes Cluster Token"
  type = string
}

variable "cluster_ca_cert" {
  description = "Kubernetes Cluster ca Certification"
  type = string
}
